#include <iostream>
#include <string>

using namespace std;

int main()
{
	string line{"A working code"};

	cout << line << "\n\n" << line.length() << "\n\n" << line[0] << " " << line[line.length() - 1] << "\n\n\n";

	cout << "This was so easy I got bored and wrote this \n\n";
}
